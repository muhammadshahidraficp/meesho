import 'dart:ui';
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ProductDetailScreen extends StatefulWidget {
  ProductDetailScreen({Key key}) : super(key: key);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  List productDetail = [
    {
      "title": "Charvi Drishya Women Sarees",
      "rating": 4.0,
    }
  ];

  List productList = [
    {
      "title": "Jivika Superior Sarees",
      "rate": "433",
      "offer": "303",
      "off": "30% off",
      "image": "assets/p1.jpeg"
    },
    {
      "title": "Myra Alluring Sarees",
      "rate": "429",
      "offer": "320",
      "off": "25% off",
      "image": "assets/p2.jpeg"
    },
    {
      "title": "Aakarsha Alluring Sarees",
      "rate": "429",
      "offer": "320",
      "off": "25% off",
      "image": "assets/p3.jpeg"
    }
  ];

  List productFeatures = [
    {"feature": "Saree Fabric: Jute Silk "},
    {"feature": "Blouse: Separate Blouse Piece "},
    {"feature": "Blouse Fabric: Jute Silk "},
    {"feature": "Pattern: Printed"},
    {"feature": "Multipack: Single"},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Icon(
          Icons.arrow_back,
          color: Colors.black,
        ),
        title: Text(
          productDetail[0]["title"],
          style: TextStyle(color: Colors.grey),
        ),
        actions: [
          Row(
            children: [
              IconButton(
                  icon: Icon(
                    Icons.search,
                    size: 30,
                    color: Colors.black,
                  ),
                  onPressed: () {}),
              IconButton(
                  icon: Icon(
                    Icons.favorite,
                    size: 30,
                    color: Colors.black,
                  ),
                  onPressed: () {}),
              IconButton(
                  icon: Icon(
                    Icons.shopping_cart,
                    size: 30,
                    color: Colors.black,
                  ),
                  onPressed: () {})
            ],
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    productDetail[0]["title"],
                    style: TextStyle(color: Colors.black, fontSize: 20),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 5),
                  child: Chip(
                    backgroundColor: Colors.lightGreen,
                    label: Row(
                      children: [
                        Text(
                          productDetail[0]["rating"].toString(),
                          style: TextStyle(color: Colors.white),
                        ),
                        Icon(
                          Icons.star,
                          size: 15,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: productFeatures.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.only(left: 10, bottom: 5),
                    child: Text(productFeatures[index]["feature"]),
                  );
                }),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                FlatButton.icon(
                  onPressed: () {},
                  icon: Icon(
                    Icons.copy,
                    color: Colors.grey,
                  ),
                  label: Text('Copy'),
                ),
                FlatButton.icon(
                  onPressed: () {},
                  icon: Icon(
                    Icons.favorite_border,
                    color: Colors.grey,
                  ),
                  label: Text('Wishlist'),
                )
              ],
            ),
            ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: productList.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Center(
                        child: Image.asset(
                          productList[index]["image"],
                          width: 210,
                          height: 210,
                        ),
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            child: Text(
                              productList[index]["title"]
                                  .toString()
                                  .toUpperCase(),
                              style:
                                  TextStyle(color: Colors.black, fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            child: Text(
                              productList[index]["offer"],
                              style:
                                  TextStyle(color: Colors.black, fontSize: 20),
                            ),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            productList[index]["rate"],
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: 17,
                                decoration: TextDecoration.lineThrough),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            productList[index]["off"],
                            style:
                                TextStyle(color: Colors.red[200], fontSize: 17),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            child: FaIcon(
                              FontAwesomeIcons.percent,
                              size: 10,
                              color: Colors.deepPurple[700],
                            ),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            '₹ 100 OFF |',
                            style: TextStyle(
                              color: Colors.deepPurple[700],
                              fontSize: 14,
                            ),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            '1st Order Discount Applied',
                            style: TextStyle(
                              color: Colors.deepPurple[700],
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            child: RaisedButton.icon(
                              onPressed: () {},
                              icon: Icon(Icons.local_shipping),
                              label: Text('Free Shipping'),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          FlatButton.icon(
                            onPressed: () {},
                            icon: FaIcon(
                              FontAwesomeIcons.shareAlt,
                              color: Colors.black,
                            ),
                            label: Text(
                              'SHARE ON OTHERS',
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                          FlatButton.icon(
                            onPressed: () {
                              showAlertDialog(
                                  context,
                                  productList[index]["image"],
                                  productList[index]["title"]);
                            },
                            icon: FaIcon(
                              FontAwesomeIcons.whatsapp,
                              color: Colors.green,
                            ),
                            label: Text(
                              'SHARE ON WHATSAPP',
                              style: TextStyle(fontSize: 12),
                            ),
                          )
                        ],
                      ),
                    ],
                  );
                }),
          ],
        ),
      ),
      floatingActionButton: RaisedButton.icon(
        color: Colors.green,
        onPressed: () {
          _shareImages();
        },
        icon: FaIcon(
          FontAwesomeIcons.whatsapp,
          color: Colors.white,
        ),
        label: Text(
          'SHARE NOW',
          style: TextStyle(color: Colors.white),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
    );
  }

  Future<void> _shareImage(imagePath, description) async {
    try {
      final ByteData bytes1 = await rootBundle.load(imagePath);
      await Share.files(
          'esys images',
          {
            'esys.png': bytes1.buffer.asUint8List(),
          },
          'image/jpeg',
          text: description);
    } catch (e) {
      print('error: $e');
    }
  }

  Future<void> _shareImages() async {
    try {
      final ByteData bytes1 = await rootBundle.load('assets/p1.jpeg');
      final ByteData bytes2 = await rootBundle.load('assets/p2.jpeg');
      final ByteData bytes3 = await rootBundle.load('assets/p3.jpeg');
      await Share.files(
          'esys images',
          {
            'esys.png': bytes1.buffer.asUint8List(),
            'bluedan.png': bytes2.buffer.asUint8List(),
            'test.png': bytes3.buffer.asUint8List(),
          },
          'image/jpeg',
          text: productDetail[0]["title"]);
    } catch (e) {
      print('error: $e');
    }
  }

  showAlertDialog(BuildContext context, imagePath, description) {
    AlertDialog alert = AlertDialog(
      title: Text(
        "Sharing Images...",
        style: TextStyle(color: Colors.grey, fontSize: 20),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Row(
              children: [
                FaIcon(
                  FontAwesomeIcons.checkCircle,
                  color: Colors.green,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Images',
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
          ),
          ListTile(
            title: Row(
              children: [
                FaIcon(
                  FontAwesomeIcons.checkCircle,
                  color: Colors.green,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Description',
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
          ),
          Divider(
            height: 5,
            thickness: 5,
            color: Colors.pink,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'All images downloaded.',
            style: TextStyle(color: Colors.grey),
          )
        ],
      ),
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
    _shareImage(imagePath, description);
  }
}
